Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: stardict
Upstream-Contact: Hu Zheng <huzheng001@gmail.com>
Source: https://gitee.com/huzheng/stardict-3.0.7
Files-Excluded:
 dict/help/stardict.omf.in
 tools/src/*.exe
 tools/src/dictgen.php
 tools/src/extractKangXi.py
 tools/src/makevietdict.py
 dict/src/dic/stardict-dict/res/wenjing.jpg
 dict/src/dic/stardict-dict/res/wenjing2.jpg
 dict/src/dic/stardict-dict/res/linux_logo.jpg
 dict/src/dic/stardict-dict/res/redhat_linux_logo.gif
 dict/src/dic/stardict-dict/res/redhat_logo.jpg
 dict/stardict-plugins/stardict-wordnet-plugin/tenis.h
Comment:
 dict/help/stardict.omf.in: needs to be removed for the build to work
                            without scrollkeeper and internet access.
 tools/src/*.exe: binary without source
 tools/src/dictgen.php: license not clear
 tools/src/extractKangXi.py: license not clear
 tools/src/makevietdict.py: license not clear
 dict/src/dic/stardict-dict/res/wenjing.jpg: a picture of a chinese girl
 dict/src/dic/stardict-dict/res/wenjing2.jpg: a picture of a chinese girl
 dict/src/dic/stardict-dict/res/linux_logo.jpg: a logo of linux
 dict/src/dic/stardict-dict/res/redhat_linux_logo.gif: logo of redhat_linux
 dict/src/dic/stardict-dict/res/redhat_logo.jpg: logo of redhat
 dict/stardict-plugins/stardict-wordnet-plugin/tenis.h: Unclear license; GPL incompatible

Files: *
Copyright: 2003, 2004, 2005, 2006, 2007 Hu Zheng
           2011 kubtek <kubtek@gmail.com>
           2011 kubtek <kubtek@mail.com>
           Evgeniy Dushistov
           Alex Murygin
License: GPL-3+

Files: *Makefile.in
Copyright: (see individual files)
License: FSFULLR
 This Makefile.in is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: dict/gnome-doc-utils.make
Copyright: 2004-2005 Shaun McCance <shaunm@gnome.org>
License: GPL-2+

Files: dict/help/*
Copyright: Hu Zheng
License: GFDL-NIV-1.2+

Files: dict/po/ga.po
Copyright: 2007 Free Software Foundation, Inc.
  Kevin Scannell <kscanne@gmail.com>, 2007.
License: GPL-3+

Files: dict/po/hu.po
Copyright: Laszlo Dvornik <dvornikl@mailbox.hu>, 2004.
  Lisovszki Sándor <lisovszki@gmail.com>, 2008.
License: GPL-3+

Files: dict/po/kk.po
Copyright: 2010 The Stardict team
  Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2010.
License: GPL-3+

Files: dict/po/ku.po
Copyright: 2007 Erdal Ronahî <erdal.ronahi@gmail.com>
License: GPL-3+

Files: dict/po/nl.po
Copyright: 2005 Free Software Foundation, Inc.
  Ruud Slaats <ruudslaats@eurotechgroup.nl>, 2005.
License: GPL-3+

Files: dict/po/pt_BR.po
Copyright: 1999 by Ma Su'an
  2002 by Opera Wang
  2003-2006 by Hu Zheng
License: GPL-3+

Files: dict/po/ru.po
Copyright: Denis Koryavov <dkoryavov@yandex.ru>, 2010
  2005 Free Software Foundation, Inc.
License: GPL-3+

Files: dict/po/rw.po
Copyright: 2005 Free Software Foundation, Inc.
  Steve Murphy <murf@e-tools.com>, 2005.
License: GPL-3+

Files: dict/po/sk.po
Copyright: 2004, 2006, 2007 Free Software Foundation, Inc.
  Zdenko Podobný <zdpo@mailbox.sk>, 2004, 2006.
  Zdenko Podobny <zdenop@gmail.com>, 2007.
  Sibyla Myslovičová <sibylam@juls.savba.sk>, 2007.
License: GPL-3+

Files: dict/po/vi.po
Copyright: 2007, Free Software Foundation.
  tam trieu <trieutritam@gmail.com>, 2004.
  Clytie Siddall <clytie@riverland.net.au>, 2005-2007.
License: GPL-3+

Files: dict/src/lib/ctype-uca.cpp
       dict/src/lib/ctype-utf8.cpp
Copyright: 2000, 2004 MySQL AB
           2011, kubtek
License: LGPL-2+

Files: dict/src/lib/dictziplib.cpp
Copyright: 1996, 1997, 1998, 2000, 2002 Rickard E. Faith (faith@dict.org)
  2003-2003 Hu Zheng <huzheng_001@163.com>
  2011 kubtek <kubtek@mail.com>
License: GPL-3+

Files: dict/src/lib/edit-distance.cpp
Copyright: Opera Wang <wangvisual AT sohu DOT com>
  2011 kubtek <kubtek@mail.com>
License: GPL-3+

Files: dict/src/sigc++/*
Copyright: 2002, 2003, 2005 The libsigc++ Development Team
License: LGPL-2.1+

Files: dict/src/eggaccelerators.*
Copyright: 2002  Red Hat, Inc.
  1998, 2001 Tim Janik
  2011 kubtek <kubtek@mail.com>
License: LGPL-2+

Files: tools/config.rpath
Copyright: 1996-2003 Free Software Foundation, Inc.
License: GPL-2+

Files: tools/src/connector.cpp
 tools/src/file.cpp
 tools/src/generator.cpp
 tools/src/generator.h
 tools/src/log.cpp
 tools/src/parser.cpp
 tools/src/parser.h
 tools/src/process.cpp
 tools/src/stardict_generator.cpp
 tools/src/utils.cpp
 tools/src/xml.cpp
Copyright: 2005, Evgeniy <dushistov@mail.ru>
  2005-2006, Evgeniy <dushistov@mail.ru>
  2006, Evgeniy <dushistov@mail.ru>
License: GPL-2+

Files: tools/src/bgl_babylonreader.cpp
 tools/src/bgl_babylonreader.h
 tools/src/bgl_dictbuilder.h
 tools/src/bgl_dictreader.h
 tools/src/bgl_stardictbuilder.cpp
 tools/src/bgl_stardictbuilder.h
Copyright: 2007, Raul Fernandes
License: GPL-2+

Files: tools/src/charset_conv.cpp
 tools/src/dsl_parser.cpp
 tools/src/normalize_tags.cpp
Copyright: Evgeniy Dushistov, 2005-2006
License: GPL-2+

Files: tools/src/hanzim2dict.py
Copyright: 2004 Michael Robinson (robinson@netrinsics.com)
License: GPL-2+

Files: tools/src/uyghur2dict.py
Copyright: 2004 Michael Robinson (robinson@netrinsics.com)
  Abdisalam (anatilim@gmail.com)
License: GPL-2+

Files: tools/src/bgl_babylon.cpp
 tools/src/bgl_babylon.h
Copyright: 2007, Raul Fernandes and Karl Grill
License: GPL-2+

Files: tools/src/mapfile.cpp
Copyright: Evgeniy Dushistov, 2006
License: GPL-2+

Files: tools/src/parse-oxford.perl
Copyright: Anthony Fok, 2006
License: GPL-2+

Files: tools/src/lingea-trd-decoder.py
Copyright: 2007, - Klokan Petr Přidal (www.klokan.cz)
License: LGPL-2+

Files: tools/src/Po2Tab.zip
Copyright: 2006 M. Bashir Al-Noimi
License: GPL-2+

Files: dict/src/win32/nsis/StarDictEditorPortable.nsi
  dict/src/win32/nsis/StarDictPortable.nsi
Copyright: John T. Haller
License: GPL-2+

Files: tools/src/libsd2foldoc.cpp
       tools/src/libsd2foldoc.h
       tools/src/sd2foldoc.cpp
Copyright: dazhiqian
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1) Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3) Neither the name of the ORGANIZATION nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GFDL-NIV-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License can be found in `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This file is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful
 but WITHOUT ANY WARRANTY; without even the implied warranty o
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License with
 the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-2;
 if not, write to the Free Software Foundation, Inc., 51 Franklin St,
 Fifth Floor, Boston, MA 02110-1301, USA.
